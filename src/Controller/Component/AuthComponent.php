<?php
namespace Riddlemd\Permissioned\Controller\Component;

use Cake\Controller\Controller;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Component\AuthComponent as BaseComponent;
use Riddlemd\Permissioned\Model\Entity\PermissionedUserInterface;
use Riddlemd\Permissioned\Auth\PermissionedAuthorize;

class AuthComponent extends BaseComponent
{
    public $_permissionAliases = [];

    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        $this->_defaultConfig = array_merge($this->_defaultConfig, [
            'authorize' => [
                'Permissioned' => [
                    'className' => 'Riddlemd/Permissioned.Permissioned'
                ]
            ],
            'authenticate' => [
                'Permissioned' => [
                    'className' => 'Riddlemd/Permissioned.Permissioned'
                ]
            ],
            'loginAction' => '/login',
            'logoutAction' => '/logout',
            'loginRedirect' => '/dashboard',
            'logoutRedirect' => '/',
            'unauthorizedRedirect' => false
        ]);

        parent::__construct($registry, $config);
    }

    protected function _isAllowed(Controller $controller)
    {
        if(parent::_isAllowed($controller))
            return true;
        
        return false;
    }

    public function addPermissionAlias($alias, $permissions)
    {
        if(!is_array($permissions))
            $permissions[] = $permissions;
        
        $this->_permissionAliases[$alias] = $permissions;
    }

    public function getPermissionsFromAlias($alias)
    {
        return $this->_permissionAliases[$alias] ?? [];
    }

    public function getUser() : ?PermissionedUserInterface
    {
        return $this->user();
    }

    public function hasAuthorization(string $permission, PermissionedUserInterface $user = null) : bool
    {
        if(!is_a($this->_authorizationProvider, PermissionedAuthorize::class)) return false;
        return $this->_authorizationProvider->hasAuthorization($permission, $user ?? $this->getUser());
    }
}