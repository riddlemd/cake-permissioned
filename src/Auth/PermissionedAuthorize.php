<?php
namespace Riddlemd\Permissioned\Auth;

use Cake\Auth\BaseAuthorize;
use Cake\Http\ServerRequest;
use Riddlemd\Permissioned\Model\Entity\PermissionedUserInterface;

class PermissionedAuthorize extends BaseAuthorize
{
    public function authorize($user, ServerRequest $request) : bool
    {
        if(!is_a($user, PermissionedUserInterface::class))
            throw new \Cake\Http\Exception\InternalErrorException(__METHOD__ . ' requires $user inherit from ' . PermissionedUserInterface::class);

        $permission = [
            'prefix' => $request->getParam('prefix') ?: null,
            'plugin' => $request->getParam('plugin') ?: 'App',
            'controller' => $request->getParam('controller'),
            'action' => $request->getParam('action'),
        ];

        $permission = 'action;' . array_reduce($permission, function($carry, $item) {
            return $carry .= $item ? ($carry ? '.' : null) . $item : null;
        });
        $permission .= isset($request->getParam('pass')[0]) ? ";{$request->getParam('pass')[0]}" : null;

        $authComponent = $this->_registry->get('Auth') ?? null;

        /*
        if(is_a($authComponent, 'Riddlemd\Controller\Component\AuthComponent'))
        {
            $aliasedPermissions = $authComponent->getPermissionsFromAlias($permission);
            
            foreach($aliasedPermissions as $aliasedPermission)
            {
                if($this->hasPermission($aliasedPermission, $user))
                    return true;
            }
        }
        */
        
        return $this->hasAuthorization($permission, $user);
    }

    public function hasAuthorization(string $permission, PermissionedUserInterface $user) : bool
    {
        if(!$user) return false;
        return $user->hasAuthorization($permission);
    }
}