<?php
namespace Riddlemd\Permissioned\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Djc\Venom\Utility\SmartArray;

class PermissionedAuthenticate extends BaseAuthenticate
{
    public function __construct(ComponentRegistry $registry , array $config = [])
    {
        parent::__construct($registry, $config);
    }

    public function authenticate(ServerRequest $request, Response $response)
    {
        $usersTable = TableRegistry::get($this->getConfig('userModel'));

        $username = $request->getData($this->getConfig('fields.username'));
        $password = $request->getData($this->getConfig('fields.password'));
        $associations = $this->getConfig('associations');

        $user = $usersTable->find()
            ->where([
                "{$usersTable->getAlias()}.{$this->getConfig('fields.username')}" => $username,
            ])
            ->contain($associations)
            ->first();

        if($user)
            return $this->passwordHasher()->check($password, $user->password) ? $user : false;
        
        return false; 
    }
}