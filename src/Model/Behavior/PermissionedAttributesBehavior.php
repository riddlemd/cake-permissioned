<?php
namespace Riddlemd\Permissioned\Model\Behavior;

use Cake\ORM\Behavior as BaseBehavior;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Validation\Validator;

class PermissionedAttributesBehavior extends BaseBehavior
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        EventManager::instance()->on('Model.afterPatchEntity', function($event, $entity, $options) {
            if(!isset($options['user']) || !is_a($options['user'], '\Riddlemd\Permissioned\Model\Entity\PermissionedUserInterface'))
                throw new \Cake\Http\Exception\InternalErrorException(__METHOD__ . ' requires $options[\'user\'] to be instance of \Riddlemd\Permissioned\Model\Entity\PermissionedUserInterface');

            if(!is_a($entity, '\Riddlemd\Permissioned\Model\Entity\PermissionedAttributesEntityInterface'))
                throw new \Cake\Http\Exception\InternalErrorException(__METHOD__ . ' requires $entity to be instance of \Riddlemd\Permissioned\Model\Entity\PermissionedAttributesEntityInterface');

            $modelAlias = $this->_table->getAlias();
            foreach($entity->getDirty() as $dirtyPropertyName)
            {
                if($entity->isPermissionedAttribute($dirtyPropertyName))
                {
                    $permission = "attribute;{$this->_table->getRegistryAlias()}.{$dirtyPropertyName}";
                    if(!$options['user']->hasAuthorization($permission))
                    {
                        $entity->$dirtyPropertyName = $entity->getOriginal($dirtyPropertyName);
                        $entity->setError($dirtyPropertyName, __('You do not have the permission to edit this attribute'));
                    }
                }
            }
        });
    }
}