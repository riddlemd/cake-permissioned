<?php
namespace Riddlemd\Permissioned\Model\Entity;

interface PermissionedUserInterface
{
    public function hasAuthorization(string $permission) : bool;
}