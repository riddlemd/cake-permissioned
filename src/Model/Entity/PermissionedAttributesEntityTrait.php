<?php
namespace Riddlemd\Permissioned\Model\Entity;

trait PermissionedAttributesEntityTrait
{
    protected $_permissionedAttributes = [
        '*' => false
    ];

    public function __debugInfo()
    {
        $properties = parent::__debugInfo();
        $properties += [
            '[permissionedAttributes]' => $this->_permissionedAttributes
        ];
        return $properties; 
    }

    public function setPermissionedAttribute(string $name, bool $requiresPermission = true) : PermissionedAttributesEntityInterface
    {
        $this->_permissionedAttributes[$name] = (bool)$requiresPermission;
        return $this;
    }

    public function isPermissionedAttribute(string $name) : bool
    {
        return $this->_permissionedAttributes[$name] ?? $this->_permissionedAttributes['*'];
    }

    public function getPermissionedAttributes() : array
    {
        return $this->_permissionedAttributes;
    }
}