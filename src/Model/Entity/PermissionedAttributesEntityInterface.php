<?php
namespace Riddlemd\Permissioned\Model\Entity;

interface PermissionedAttributesEntityInterface
{
    public function setPermissionedAttribute(string $name, bool $requiresPermission = true) : self;
    public function isPermissionedAttribute(string $name) : bool;
    public function getPermissionedAttributes() : array;
}