<?php
namespace Riddlemd\Permissioned\View\Helper;

use Riddlemd\Tools\View\Helper\FormHelper as BaseHelper;

class PermissionAwareFormHelper extends BaseHelper
{
    protected function _initInputField($fieldName, $options = [])
    {
        $options = parent::_initInputField($fieldName, $options);

        $context = $this->_getContext();
        if(is_a($context, '\Cake\View\Form\EntityContext'))
        {
            $entity = $context->entity();
            
            if(is_a($entity, '\Riddlemd\Permissioned\Entity\PermissionedAttributesEntityInterface') && $entity->isPermissionedAttribute($fieldName))
            {
                if($this->_currentUser === null)
                    $this->_currentUser = $this->request->session()->read('Auth.User') ?? false;

                $permission = "attribute;{$entity->getSource()}.{$fieldName}";
                $options['data-requires-permission'] = true;
                $options['data-user-has-permission'] = $this->_currentUser ? $this->_currentUser->hasAuthorization($permission) : 0;
                $options['title'] = 'You do not have permission to edit this field';
            }
        }

        return $options;
    }
}